FROM hashicorp/terraform:1.1.7

ENV ANSIBLE_VERSION=2.10.7

ENV TERRAGRUNT=v0.36.6

ADD https://github.com/gruntwork-io/terragrunt/releases/download/${TERRAGRUNT}/terragrunt_linux_amd64 /usr/local/bin/terragrunt


RUN echo "===> Configure permission for terragrunt..." && \
	chmod +x /usr/local/bin/terragrunt && \
	\
	echo "===> Installing Python..." && \
	apk --update add python3 \
		py3-pip \
		openssl \
		ca-certificates \
		curl \
		bash && \
	apk --update add --virtual .build-deps \
		python3-dev \
		libffi-dev \
		openssl-dev \
		gcc \
		musl-dev \
		cargo \
		build-base \
		openssh-client \
		openssh-server && \
	pip3 install --upgrade pip cffi && \
	\
	echo "===> Installing Ansible and Boto Library..." && \
	pip3 install ansible==${ANSIBLE_VERSION} awscli botocore boto3 && \
	\
	apk del .build-deps && \
	rm -rf /var/cache/apk/* && \
	\
	echo "===> Adding localhost to hosts file..." && \
	mkdir -p /etc/ansible && \
	echo 'localhost' > /etc/ansible/hosts



RUN curl -sSL https://sdk.cloud.google.com | bash

ENV PATH $PATH:/root/google-cloud-sdk/bin

RUN apk add --no-cache jq

COPY playbooks/ /app/

ENTRYPOINT ["sh", "-c", "terraform -v && terragrunt -v && ansible --version && aws --version"]
